const path = require('path')

module.exports = {
  testRegex: '(/tests/[^(helpers|.)].*|(\\.|/)(test|spec))\\.js$',
  rootDir: path.resolve(__dirname, '../../'),
  setupFiles: ['<rootDir>/tests/helpers/setup.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/config/jest/fileMock.js',
  },
}
