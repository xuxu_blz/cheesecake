const webpack = require('webpack')
const path = require('path')

const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          { loader: 'image-webpack-loader', options: { disable: true } },
        ],
      },
    ],
  },

  entry: {
    app: './src/app.js',
    vendor: ['react', 'react-dom', 'react-router-dom', 'styled-components'],
  },

  output: {
    filename: '[name].[chunkhash].js',
    path: path.resolve(__dirname, '../../dist'),
    publicPath: '/',
  },

  plugins: [
    new HTMLWebpackPlugin({ template: './src/html/index.ejs' }),
    new Dotenv({ systemVars: true }),
    new UglifyJSPlugin(),
    new CopyWebpackPlugin([{ from: './static/**/*', to: './' }]),
  ],

  mode: 'production',

  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      minChunks: 1,
      name: true,

      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true,
        },
      },
    },
  },
}
