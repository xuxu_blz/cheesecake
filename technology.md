# Technology

## Why not Create-React-App/Next.js?

While I do love Next.js as a framework and would probably use it to start any
new production project, I decided that it would be more interesting for a
recruitment challenge to make it clear that I can properly configure a React
project from scratch, including Webpack and Babel configurations.

## React

This application is built on React version 16.6, I've considered using the
16.7 version and using the new hoooks API in my code, but decided it could be
too risky, considering the short time available to develop this project.

Since the application employs very minimal global state, I decided to handle
all of the state lifting with React's context API, the `AuthProvider` component
(in `scr/react/components/Auth.js`) creates and provides the authentication
data to all it's children, while the `Auth` component (in the same file)
subscribes to the context and passes it down to its children through a render
props/children as functions pattern.

The render props pattern is also used in the `Resource` component
(`src/react/components/Resource.js`), it takes advantage of the fact that
all API resource functions follow the same contract by receiving one of these
functions as a prop and rendering it's children (as functions) with the received
data or error.

## Google Spreadsheets API

I decided that I wanted to have at least a small piece of code that shows how I
prefer to handle http requests, so I needed to have some type of "server"
available somewhere.

The Google Spreadsheets API serves as a good tool to help me develop a prototype
with dynamic data quickly, I can easily edit the date from Google Drive and see
a different API response immediately.

The API's response is always an array that represents the lines, the lines are
represented as an array of strings, each string contains the value of a cell.
To transform that data into an object that better represents the shape of the
data I'm using, I use the first line as a "header" that defines the key names
for all the values. This requires that the loaded file strictly follows the
defined format and uses the same names as it's headings.

The project uses an "index" spreadsheet that contains data for all the posts
([example](https://docs.google.com/spreadsheets/d/1eNmEYlZ0dT_5PhxFL15K7fkrp45xuo6OC0PNdLT2-zk/edit#gid=0))
and each posts refer to a "single" spreadsheet that contains the post's details
and full content ([example](https://docs.google.com/spreadsheets/d/1mlHDOZoaz4vZgAuKfQ-wvTIeH6j7_2_r9A_0Sl6zFT0/edit#gid=0)).

## Fetching data

This projects uses the browser's fetch API instead of requiring an npm library
such as axios, the necessary polyfills for older browsers are provided by Babel.

I build on the idea of not rejecting the promise on error in my code and always
resolve with a two-value array (kind of like a tuple) that will always contain
a possible error and a possible response, the consuming code can then check for
the existence of the error or response and then act accordingly. This avoids
some risks of an unexpected error resulting in a rejected promise with different
error data causing the app to break.

## Flow

Flow lets me add static type annotations to my code and guarantees that I'm
always using my data correctly (as long as I manage to type them correctly),
this helps me to avoid shipping bugs that could be hard to spot. The best
example in this project is the `Resource` component, which is capable of
correctly guessing the types its children can consume from the type information
contained in the received resource function, and print error messages when
a child component expects incorect prop types.

TypeScript is definitely becoming the more popular option right now, but I still
have a personal preference towards flow. Flow is designed to be easy to adopt
in a progressive manner (specially in an old, untyped project), and is slightly
better at inferring types.

For a real, production project, TypeScript is probably a safer option.

## Styled Components

Adopting a CSS-in-JS approach helps me build more self-contained components that
can carry their own styles with them, while not accidentally affecting the
styles of other unrelated components. It also lets me dynamically add styling
from JavaScript logic without the need of adding extra classNames.

The other big reason why I enjoy writing styles with styled-components is that
it allows me to write CSS in a style that is very close to classic SCSS, which
I'm most comfortable with.

The more presentational components that are reused through the application and
don't offer much functionality apart from their styling can sometimes be build
with `styled-components` alone (no React import), and are usually available in
a `primitives` folder in my projects.

## Jest

This project uses Jest for unit testing, not all components and functions in the
project are tested (mainly due to time constraints). React components are
rendered with AirBNB's enzyme, and API requests are caught with a mock server
created with Nock. Some components have more detailed behavior tests, but most
presentational components are simply tested with Jest snapshots.