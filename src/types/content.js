// @flow strict
import { theme } from '../styles/theme'

export type Category = 'politics' | 'business' | 'tech' | 'science' | 'sports'

export const allCategories = [
  'politics',
  'business',
  'tech',
  'science',
  'sports',
]

export const categoryColors = {
  politics: theme.red,
  business: theme.purple,
  tech: theme.blue,
  science: theme.green,
  sports: theme.yellow,
}
