// @flow strict
export const theme = {
  black: '#000',
  textColor: '#686868',
  linkColor: '#000',
  red: '#D0021B',
  purple: '#BD10E0',
  blue: '#4990E2',
  green: '#7CBB37',
  yellow: '#F5A623',
  primaryColor: '#4990E2',
  hoverColor: '#5E9BE2',
  backgroundColor: '#FFFFFF',
  danger: '#C10016',
}
