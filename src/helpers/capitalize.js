// @flow strict
export const capitalize = (word: string) =>
  word.replace(/\b\w/g, firstCharacter => firstCharacter.toUpperCase())
