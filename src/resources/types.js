// @flow strict
export type NetworkError = { status: number, message: string }
