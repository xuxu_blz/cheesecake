// @flow strict
/* global process */
import type { NetworkError } from './types'
import type { User } from '../react/components/Auth'

const fakeUser = { id: 'a12nxcz38fad', name: 'Cheesecake', interests: [] }
const VALID_USER = process.env.VALID_USER || 'nothing'
const VALID_PASS = process.env.VALID_PASS || '0000000'

type LoginParams = { username: string, password: string }

export const logIn = ({
  username,
  password,
}: LoginParams): Promise<[?NetworkError, ?User]> =>
  new Promise(resolve => {
    setTimeout(() => {
      if (username === VALID_USER && password === VALID_PASS) {
        resolve([null, fakeUser])
      } else {
        resolve([
          {
            status: 401,
            message: 'Either the username or password you provided is invalid',
          },
          null,
        ])
      }
    }, 1000)
  })
