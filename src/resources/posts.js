// @flow
/* global process */
import { fetchSheet } from './googleSheets'

import type { NetworkError } from './types'
const API_KEY = process.env.GOOGLE_API_KEY || 'noKey'
const INDEX_ID = process.env.GOOGLE_INDEX_SHEET_ID || 'noID'

export type ShortPost = {
  id: string,
  title: string,
  excerpt: string,
  thumbnail: string,
  author_name: string,
  author_image: string,
  category: string,
}

export type IndexResponse = {
  posts: Array<ShortPost>,
}

export type FullPost = {
  id: string,
  title: string,
  content: string,
  thumbnail: string,
  author_name: string,
  author_image: string,
  category: string,
}

export type SingleResponse = {
  posts: [FullPost],
}

export const getPosts = (): Promise<[?NetworkError, ?IndexResponse]> =>
  fetchSheet({ id: INDEX_ID, range: 'A1:G31', key: API_KEY })

export const getPost = (
  id: ?string
): Promise<[?NetworkError, ?SingleResponse]> =>
  fetchSheet({ id, range: 'A1:G2', key: API_KEY })
