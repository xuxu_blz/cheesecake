import { pipe, map, head, tail, fromPairs } from 'ramda'

import { to } from '../helpers/async'

const getErrorMessage = status => {
  switch (status) {
    case 404:
      return 'There is no data available for the requested ID, try navigating to another post'
    case 401:
      return 'The API has denied your access to this post, check if your API Token is still valid'
    default:
      return 'The API has responded with an error, check if your API Token is still valid'
  }
}

const parseSheet = ({ values }) => {
  const keys = head(values)

  return {
    posts: pipe(
      tail,
      map(values => values.map((value, i) => [keys[i], value])),
      map(fromPairs)
    )(values),
  }
}

export const fetchSheet = async ({ id, range, key }) => {
  const [error, response] = await to(
    fetch(
      `https://sheets.googleapis.com/v4/spreadsheets/${id}/values/${range}?key=${key}`
    )
  )

  if (error) {
    return [
      {
        status: 0,
        message:
          'Something prevented the data request from being fulfilled, please check your internet connection',
      },
    ]
  }

  const data = await response.json()

  if (response.status === 200) {
    return [null, parseSheet(data)]
  } else {
    return [
      {
        status: response.status,
        message: getErrorMessage(response.status),
      },
    ]
  }
}
