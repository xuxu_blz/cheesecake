// @flow
import React from 'react'
import { render } from 'react-dom'
import { ThemeProvider } from 'styled-components'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import { theme } from './styles/theme.js'
import GlobalStyle from './styles/GlobalStyle'
import DefaultTemplate from './react/templates/Default'
import Main from './react/pages/Main'
import Single from './react/pages/Single'
import NotFound from './react/pages/NotFound'
import { AuthProvider } from './react/components/Auth'
import ErrorBoundary from './react/components/ErrorBoundary'
import '@babel/polyfill'

const container = document.getElementById('app')
if (container) {
  render(
    <ThemeProvider theme={theme}>
      <ErrorBoundary>
        <AuthProvider>
          <GlobalStyle />
          <BrowserRouter>
            <DefaultTemplate>
              <Switch>
                <Route exact path="/" component={Main} />
                <Route exact path="/category/:name" component={Main} />
                <Route path="/post/:id" component={Single} />
                <Route path="/*" component={NotFound} />
              </Switch>
            </DefaultTemplate>
          </BrowserRouter>
        </AuthProvider>
      </ErrorBoundary>
    </ThemeProvider>,
    container
  )
}
