// @flow
import React from 'react'
import styled from 'styled-components'

import Button from '../primitives/Button'
import { logIn } from '../../resources/auth'
import type { NetworkError } from '../../resources/types'
import type { User } from './Auth'

const LoginForm = styled.form`
  text-align: center;
  max-width: 285px;
  margin: 0 auto;
  padding: 0 20px;

  &[aria-busy='true'] {
    opacity: 0.6;
  }

  h3 {
    font-weight: 700;
    font-size: 24px;
    margin: 2em 0;
    text-transform: uppercase;
    color: #3e433e;
  }

  p {
    margin-top: -34px;
    color: ${({ theme, error }) => (error ? theme.danger : theme.textColor)};
  }

  fieldset {
    border: 0;
  }

  label {
    display: block;
    text-transform: uppercase;
    text-align: left;
    margin: 2em 0;
    color: ${({ theme, error }) => (error ? theme.danger : theme.textColor)};
  }

  input {
    display: block;
    text-transform: none;
    background: transparent;
    -webkit-appearance: none;
    -moz-appearance: none;
    transition: border 0.3s linear;
    border: 1px solid
      ${({ error, theme }) => (error ? theme.danger : '#bfbfbf')};
    height: 40px;
    width: 100%;
    font-size: 18px;
    padding: 0 0.4em;
    margin: 0.4em 0;

    &:focus {
      border-color: ${({ theme }) => theme.primaryColor};
    }
  }

  a {
    color: ${({ theme }) => theme.primaryColor};
    text-transform: uppercase;
    margin: 1em 0;
  }
`

type Props = { onSuccess: User => void, onClose: () => void }

type State = {
  username: string,
  password: string,
  loading: boolean,
  error: ?NetworkError,
}

class Login extends React.Component<Props, State> {
  state = { username: '', password: '', loading: false, error: null }

  handleInputChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({ [e.target.name]: e.target.value, error: null })
  }

  handleSubmit = async (e: SyntheticEvent<HTMLFormElement>) => {
    const { username, password } = this.state
    e.preventDefault()

    this.setState({ loading: true, error: null })

    const [error, data] = await logIn({ username, password })

    if (error) {
      this.setState({ error, loading: false })
    }

    if (data) {
      this.setState({ loading: false }, () => {
        this.props.onSuccess(data)
      })
    }
  }

  handleClickBack = (e: SyntheticMouseEvent<HTMLAnchorElement>) => {
    e.preventDefault()
    this.props.onClose()
  }

  render() {
    const { loading, error, username, password } = this.state

    return (
      <LoginForm onSubmit={this.handleSubmit} aria-busy={loading} error={error}>
        <h3>User Area</h3>

        {error && <p>{error.message}</p>}

        <fieldset>
          <label>
            Username
            <input
              type="text"
              name="username"
              value={username}
              onChange={this.handleInputChange}
            />
          </label>

          <label>
            Password
            <input
              type="password"
              name="password"
              value={password}
              onChange={this.handleInputChange}
            />
          </label>

          <Button type="submit" disabled={error || loading} aria-busy={loading}>
            {loading ? 'Submitting' : error ? 'Error' : 'Login'}
          </Button>

          <a href="#" onClick={this.handleClickBack}>
            Back to home
          </a>
        </fieldset>
      </LoginForm>
    )
  }
}

export default Login
