// @flow
import React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'

import type { NetworkError } from '../../resources/types'

const Container = styled.article`
  max-width: 860px;
  margin: 100px auto 0;
  text-align: center;

  h1 {
    font-size: 32px;
  }

  p {
    font-size: 18px;
  }
`

type Props = { error: NetworkError }

const ErrorMessage = ({ error }: Props) => (
  <Container>
    <Helmet>
      <title>Error processing request | Cheesecake</title>
    </Helmet>

    <h1>There was an error processing your request!</h1>
    <p>{error.message}</p>
  </Container>
)

export default ErrorMessage
