// @flow
import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { categoryColors, type Category } from '../../types/content'
import Auth from './Auth'
import Profile from './Profile'

import logo from '../../assets/logo.png'
import logo2x from '../../assets/logo@2x.png'

const Container = styled.header`
  background: ${({ theme }) => theme.backgroundColor};
  position: relative;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  width: 100%;
  height: 55px;
  border-bottom: 1px solid #979797;

  .header-wrap {
    display: grid;
    grid-template-columns: 100px 1fr 100px;
  }

  @media screen and (min-width: 768px) {
    .header-wrap {
      max-width: 1080px;
      padding: 0 20px;
      margin: 0 auto;
      grid-template-columns: 45px 1fr;
    }
  }
`

const Hamburger = styled.button`
  display: inline-block;
  width: 24px;
  height: 19px;
  background: transparent;
  border: 0;
  position: relative;
  cursor: pointer;
  margin: 18px 0 0 32px;

  i {
    display: block;
    width: 100%;
    height: 3px;
    background: ${({ theme }) => theme.black};
    position: absolute;
    top: 50%;
    left: 0;
    margin-top: -1.5px;
    pointer-events: none;

    &:first-child {
      top: 0;
      margin-top: 0;
    }

    &:last-child {
      top: auto;
      bottom: 0;
      margin-top: 0;
    }
  }

  @media screen and (min-width: 768px) {
    display: none;
  }
`

const Logo = styled.h1`
  text-align: center;

  a {
    margin-top: 4px;
    text-indent: -9999em;
    display: inline-block;
    width: 45px;
    height: 45px;
    background: url(${logo}) no-repeat center center;
  }

  @media screen and (min-device-pixel-ratio: 1.25),
    screen and (min-resolution: 200dpi),
    screen and (min-resolution: 1.25dppx) {
    a {
      background-image: url(${logo2x});
      background-size: 45px;
    }
  }
`

const Menu = styled.nav`
  position: absolute;
  top: 55px;
  left: 0;
  overflow-y: scroll;
  transition: height 0.3s ease-in-out;
  background: ${({ theme }) => theme.backgroundColor};
  width: 100%;
  height: ${({ show }) => (show ? 'calc(100vh - 55px)' : 0)};
  padding-bottom: ${({ show }) => (show ? '30px' : 0)};

  li {
    margin: 14px auto 0;

    a {
      text-transform: uppercase;
      display: block;
      padding-left: 32px;
      line-height: 32px;
    }
  }

  @media screen and (min-width: 768px) {
    height: auto;
    background: transparent;
    position: static;
    text-align: right;
    padding: 0;

    li {
      display: inline-block;

      a {
        padding: 0 16px;
      }
    }
  }

  @media screen and (min-height: 560px) {
    overflow: auto;
  }
`

export const CategoryItem = styled.li`
  a,
  a:hover {
    color: ${({ active, color }) => (active ? color : 'black')};
    font-weight: ${({ active }) => (active ? '700' : '400')};
  }
`

export const ProfileButton = styled.button`
  text-transform: uppercase;
  font-size: 14px;
  font-weight: 700;
  border: 0;
  background: transparent;
  display: block;
  padding-left: 32px;
  line-height: 32px;
  color: ${({ theme }) => theme.primaryColor};
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`

type Props = { categories: Array<Category>, selectedCategory: ?Category }
type State = { showProfile: boolean, showMenu: boolean }

class Header extends React.Component<Props, State> {
  state = { showProfile: false, showMenu: false }

  toggleShowProfile = () => {
    this.setState({ showProfile: !this.state.showProfile })
  }

  toggleShowMenu = () => {
    this.setState({ showMenu: !this.state.showMenu })
  }

  render() {
    const { categories, selectedCategory } = this.props
    const { showMenu, showProfile } = this.state

    return (
      <Auth>
        {({ user, logIn, logOut, updateInterests }) => (
          <Container>
            <div className="header-wrap">
              <Hamburger
                title={showMenu ? 'Hide menu' : 'Open menu'}
                onClick={this.toggleShowMenu}
              >
                <i />
                <i />
                <i />
              </Hamburger>

              <Logo>
                <Link to="/">Cheesecake</Link>
              </Logo>

              <Menu show={showMenu}>
                <ul>
                  {categories.map(category => (
                    <CategoryItem
                      key={category}
                      active={category === selectedCategory}
                      color={categoryColors[category]}
                    >
                      <Link
                        to={`/category/${category}`}
                        onClick={this.toggleShowMenu}
                      >
                        {category}
                      </Link>
                    </CategoryItem>
                  ))}
                  <li>
                    <ProfileButton onClick={this.toggleShowProfile}>
                      {user ? user.name : 'Login'}
                    </ProfileButton>
                  </li>
                </ul>
              </Menu>

              <Profile
                visible={showProfile}
                user={user}
                logIn={logIn}
                logOut={logOut}
                updateInterests={updateInterests}
                onClose={this.toggleShowProfile}
              />
            </div>
          </Container>
        )}
      </Auth>
    )
  }
}

export default Header
