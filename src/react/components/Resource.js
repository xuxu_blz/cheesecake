// @flow
import * as React from 'react'

import { newValues } from '../../helpers/compareObjects.js'
import type { NetworkError } from '../../resources/types'

class Resource<P, D> extends React.Component<
  {
    get: (?P) => Promise<[?NetworkError, ?D]>,
    params?: P,
    children: ({
      loading: boolean,
      error: ?NetworkError,
      data: ?D,
    }) => React.Node,
  },
  { loading: boolean, data: any, error: ?NetworkError }
> {
  state = { loading: false, error: null, data: null }

  componentDidMount() {
    this.fetch()
  }

  componentDidUpdate(prevProps: { params?: P }) {
    if (newValues(this.props.params, prevProps.params).length) {
      this.setState({ data: null })
      this.fetch()
    }
  }

  fetch = () => {
    this.setState({ loading: true })

    this.props.get(this.props.params).then(([error, data]) => {
      this.setState({ loading: false, error, data })
    })
  }

  render() {
    const { loading, data, error } = this.state
    return this.props.children({ loading, data, error })
  }
}

export default Resource
