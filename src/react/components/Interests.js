// @flow
import React from 'react'
import { indexOf, append, without } from 'ramda'
import styled from 'styled-components'

import Toggle from '../primitives/Toggle'
import Button from '../primitives/Button'
import type { User } from './Auth'
import {
  allCategories,
  categoryColors,
  type Category,
} from '../../types/content'

const InterestsForm = styled.form`
  margin: 0 auto;
  text-align: center;
  max-width: 580px;
  padding: 0 20px;

  fieldset {
    border: 0;
  }

  h3 {
    font-size: 24px;
    font-weight: 700;
    text-transform: uppercase;
    margin: 2em 0;

    strong {
      color: ${({ theme }) => theme.primaryColor};
    }
  }

  h4 {
    font-size: 14px;
    text-align: left;
    text-transform: uppercase;
    color: #3e433e;
    margin-bottom: 1em;
  }

  label {
    margin: 0 1em 1em 0;
  }

  button {
    margin: 2.5em auto 1.5em;
  }

  a {
    display: block;
    text-transform: uppercase;
    color: ${({ theme }) => theme.primaryColor};
    margin: 1em;
  }
`

const addOrRemove = (
  category: Category,
  categories: Array<Category>
): Array<Category> => {
  if (indexOf(category, categories) >= 0) {
    return without([category], categories)
  } else {
    return append(category, categories)
  }
}

type Props = {
  user: User,
  updateInterests: (Array<Category>) => void,
  logOut: () => void,
  onClose: () => void,
}

type State = { interests: Array<Category> }

class Interests extends React.Component<Props, State> {
  state = { interests: [] }

  componentDidMount() {
    this.setState({ interests: this.props.user.interests })
  }

  handleChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const { name } = e.target
    // flow forces me to validate the input name so it
    // knows its an actual valid category name
    // @TODO: find a way around this
    if (
      name === 'politics' ||
      name === 'business' ||
      name === 'science' ||
      name === 'tech' ||
      name === 'sports'
    ) {
      this.setState({
        interests: addOrRemove(name, this.state.interests),
      })
    }
  }

  handleSubmit = (e: SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault()
    this.props.updateInterests(this.state.interests)
    this.props.onClose()
  }

  handleClickBack = (e: SyntheticMouseEvent<HTMLAnchorElement>) => {
    e.preventDefault()
    this.props.onClose()
  }

  handleClickLogOut = (e: SyntheticMouseEvent<HTMLAnchorElement>) => {
    e.preventDefault()
    this.props.logOut()
  }

  render() {
    const { user } = this.props

    return (
      <InterestsForm onSubmit={this.handleSubmit}>
        <h3>
          Welcome, <strong>{user.name}</strong>
        </h3>

        <fieldset>
          <h4>My interests</h4>

          {allCategories.map(category => (
            <Toggle
              key={category}
              name={category}
              onChange={this.handleChange}
              checked={indexOf(category, this.state.interests) >= 0}
              color={categoryColors[category]}
            />
          ))}

          <Button type="submit">save</Button>
        </fieldset>

        <a href="#" onClick={this.handleClickBack}>
          Back to home
        </a>

        <a href="#" onClick={this.handleClickLogOut}>
          Log out
        </a>
      </InterestsForm>
    )
  }
}

export default Interests
