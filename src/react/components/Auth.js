// @flow strict
import * as React from 'react'

import type { Category } from '../../types/content'

export type User = {
  id: string,
  name: string,
  interests: Array<Category>,
}

export type Auth = {
  user: ?User,
  logIn: User => void,
  logOut: () => void,
  updateInterests: (Array<Category>) => void,
}

export const AuthContext = React.createContext<Auth>({
  user: null,
  logIn: () => {},
  logOut: () => {},
  updateInterests: () => {},
})

type PProps = { children: React.Node }
type PState = { user: ?User }

export class AuthProvider extends React.Component<PProps, PState> {
  state = { user: null }

  componentDidMount() {
    const userJson = localStorage.getItem('_cheesecake_user')

    if (userJson) {
      this.setState({ user: JSON.parse(userJson) })
    }
  }

  handleLogIn = (user: User) => {
    this.setState({ user })
    localStorage.setItem('_cheesecake_user', JSON.stringify(user))
  }

  handleLogOut = () => {
    this.setState({ user: null })
    localStorage.setItem('_cheesecake_user', JSON.stringify(null))
  }

  handleUpdateInterests = (interests: Array<Category>) => {
    this.setState({ user: { ...this.state.user, interests } })
    localStorage.setItem(
      '_cheesecake_user',
      JSON.stringify({ ...this.state.user, interests })
    )
  }

  render() {
    return (
      <AuthContext.Provider
        value={{
          ...this.state,
          logIn: this.handleLogIn,
          logOut: this.handleLogOut,
          updateInterests: this.handleUpdateInterests,
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}

type CProps = { children: Auth => React.Node }

class AuthContextConsumer extends React.Component<CProps> {
  static contextType = AuthContext

  render() {
    return this.props.children(this.context)
  }
}

export default AuthContextConsumer
