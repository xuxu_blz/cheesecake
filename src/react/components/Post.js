// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Author from '../primitives/Author'
import CategoryName from '../primitives/CategoryName'
import type { ShortPost } from '../../resources/posts'

const Container = styled.article`
  header {
    margin-bottom: 20px;

    h2 {
      font-size: 30px;
      margin: 15px 0 10px;
    }
  }

  p {
    font-family: 'Open Sans', sans-serif;
  }

  @media screen and (min-width: 480px) {
    grid-column: ${({ index }) => (index === 0 ? 'span 2' : 'span 1')};

    header {
      h2 {
        font-size: ${({ index }) => (index === 0 ? '30px' : '18px')};
      }
    }
  }

  @media screen and (min-width: 768px) {
    grid-column: ${({ index }) =>
      index === 0 ? 'span 6' : index < 3 ? 'span 3' : 'span 4'};
  }
`

const Thumbnail = styled(Link)`
  display: block;
  position: relative;
  width: 100%;
  padding-bottom: 63.888%;
  background: #eee;

  img {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
  }

  @media screen and (min-width: 768px) {
    &:after {
      content: 'Read More';
      display: block;
      width: 150px;
      height: 45px;
      position: absolute;
      top: 50%;
      left: 50%;
      margin-top: -23px;
      margin-left: -75px;
      border: 1px solid #fff;
      font-size: 18px;
      line-height: 45px;
      text-align: center;
      color: #fff;
      background: rgba(0, 0, 0, 0.5);
      opacity: 0;
      transition: all 0.2s linear;
      transform: scale(1.2);
      filter: blur(5px);

      &:hover {
        text-decoration: underline;
      }
    }
  }

  &:hover:after {
    opacity: 1;
    transform: scale(1);
    filter: blur(0px);
  }
`

type Props = {
  post: ShortPost,
  index: number,
}

const Post = ({ post, index }: Props) => (
  <Container index={index}>
    <header>
      <CategoryName category={post.category} />

      {index < 3 && (
        <Thumbnail to={`/post/${post.id}`}>
          <img src={post.thumbnail} alt={post.title} />
        </Thumbnail>
      )}

      <h2>
        <Link to={`/post/${post.id}`}>{post.title}</Link>
      </h2>

      <Author
        size={index === 0 ? '45px' : '32px'}
        name={post.author_name}
        image={post.author_image}
      />
    </header>
    {index > 0 && <p>{post.excerpt}</p>}
  </Container>
)

export default Post
