// @flow
import React from 'react'
import { filter, take, pipe } from 'ramda'
import styled from 'styled-components'

import Post from './Post'
import type { ShortPost } from '../../resources/posts'
import type { Category } from '../../types/content'

export const Container = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 40px;

  @media screen and (min-width: 480px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 45px;
  }

  @media screen and (min-width: 768px) {
    grid-template-columns: repeat(12, 1fr);
    grid-gap: 30px;
  }
`

export const Divider = styled.hr`
  display: none;

  @media screen and (min-width: 768px) {
    display: block;
    grid-column: span 12;
    border: 1px solid #e4e4e4;
    border-bottom: 0;
  }
`

type Props = {
  posts: Array<ShortPost>,
  categories: Array<Category>,
}

const PostsList = ({ posts, categories }: Props) => {
  const filteredPosts = pipe(
    filter(post => categories.indexOf(post.category) >= 0),
    take(6)
  )(posts)

  return (
    <Container>
      {filteredPosts.length ? (
        filteredPosts.map((post, i) => (
          <React.Fragment key={post.id}>
            {i === 3 && <Divider />}
            <Post post={post} index={i} />
          </React.Fragment>
        ))
      ) : (
        <p>
          There are no posts for your category selection, try navigating to
          another page
        </p>
      )}
    </Container>
  )
}

export default PostsList
