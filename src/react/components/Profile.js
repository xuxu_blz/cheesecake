// @flow
import React from 'react'
import styled from 'styled-components'

import type { User } from './Auth'
import type { Category } from '../../types/content'
import Login from './Login'
import Interests from './Interests'

const Container = styled.aside`
  background: ${({ theme }) => theme.backgroundColor};
  position: absolute;
  top: 56px;
  left: 0;
  overflow-y: scroll;
  transition: height 0.3s ease-in-out;
  width: 100%;
  height: ${({ show }) => (show ? 'calc(100vh - 55px)' : 0)};

  @media screen and (min-height: 560px) {
    overflow: auto;
  }
`

type Props = {
  visible: boolean,
  user: ?User,
  logIn: User => void,
  logOut: () => void,
  updateInterests: (Array<Category>) => void,
  onClose: () => void,
}

const Profile = ({
  visible,
  user,
  logIn,
  logOut,
  updateInterests,
  onClose,
}: Props) => (
  <Container show={visible}>
    {user ? (
      <Interests
        user={user}
        updateInterests={updateInterests}
        logOut={logOut}
        onClose={onClose}
      />
    ) : (
      <Login onSuccess={logIn} onClose={onClose} />
    )}
  </Container>
)

export default Profile
