// @flow
import React from 'react'

import { Container, Divider } from './PostsList'
import PostPlaceholder from '../primitives/PostPlaceholder'

const PostsPlaceholder = () => (
  <Container>
    <PostPlaceholder index={0} />
    <PostPlaceholder index={1} />
    <PostPlaceholder index={2} />
    <Divider />
    <PostPlaceholder index={3} />
    <PostPlaceholder index={4} />
    <PostPlaceholder index={5} />
  </Container>
)

export default PostsPlaceholder
