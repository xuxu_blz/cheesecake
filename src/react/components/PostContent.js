// @flow
import React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'

import CategoryName from '../primitives/CategoryName'
import Author from '../primitives/Author'
import type { FullPost } from '../../resources/posts'
import { capitalize } from '../../helpers/capitalize.js'

const Container = styled.article`
  max-width: 860px;
  margin: 0 auto;

  header {
    margin-bottom: 20px;

    h1 {
      color: ${({ theme }) => theme.linkColor};
      font-size: 30px;
      margin: 15px 0 10px;
    }

    p {
      font-family: 'Open Sans', sans-serif;
    }

    img {
      display: block;
      width: 100%;
      height: auto;
    }
  }

  section {
    font-family: 'Open Sans', sans-serif;

    p {
      margin: 1.6em 0;
    }
  }

  @media screen and (min-width: 768px) {
    header {
      h1 {
        font-size: 48px;
      }
    }

    section {
      font-size: 18px;
    }
  }
`
const Thumbnail = styled.div`
  display: block;
  position: relative;
  width: 100%;
  padding-bottom: 63.888%;
  background: #eee;

  img {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
  }
`

type Props = {
  post: FullPost,
}

const PostContent = ({ post }: Props) => (
  <Container>
    <Helmet>
      <title>
        {post.title} | {capitalize(post.category)} News | Cheesecake
      </title>
    </Helmet>
    <header>
      <CategoryName category={post.category} />

      <Thumbnail>
        <img src={post.thumbnail} alt={post.title} />
      </Thumbnail>

      <h1>{post.title}</h1>

      <Author size="45px" name={post.author_name} image={post.author_image} />
    </header>
    <section dangerouslySetInnerHTML={{ __html: post.content }} />
  </Container>
)

export default PostContent
