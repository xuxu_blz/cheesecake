// @flow
import * as React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'

import Header from '../components/Header'

import { allCategories } from '../../types/content'
import { capitalize } from '../../helpers/capitalize'

const Main = styled.main`
  padding: 110px 20px 55px;
  max-width: 1180px;
  margin: 0 auto;
  min-height: 90vh;

  @media screen and (min-width: 768px) {
    padding-top: 130px;
    padding-bottom: 76px;
  }
`

const Footer = styled.footer`
  text-align: right;
  padding: 20px;
`

type Props = { children: React.Node }

const DefaultTemplate = ({ children }: Props) => {
  const match = window.location.pathname.match(/^\/category\/(.*)?\/?$/)

  return (
    <>
      <Helmet>
        <title>
          {match ? `${capitalize(match[1])} News | Cheesecake` : 'Cheesecake'}
        </title>
      </Helmet>
      <Header categories={allCategories} selectedCategory={match && match[1]} />
      <Main>{children}</Main>
      <Footer>&copy; 2018 no copyright whatsoever</Footer>
    </>
  )
}

export default DefaultTemplate
