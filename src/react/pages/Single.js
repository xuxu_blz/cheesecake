// @flow
import React from 'react'
import type { Match } from 'react-router-dom'

import Resource from '../components/Resource'
import PostContent from '../components/PostContent'
import PostPlaceholder from '../primitives/PostPlaceholder'
import ErrorMessage from '../components/ErrorMessage'
import { getPost } from '../../resources/posts'

type Props = { match: Match }

const Single = ({ match }: Props) => (
  <Resource get={getPost} params={match.params.id}>
    {({ loading, error, data }) => {
      if (loading) return <PostPlaceholder index={0} />

      if (error) return <ErrorMessage error={error} />

      if (data) return <PostContent post={data.posts[0]} />

      return null
    }}
  </Resource>
)

export default Single
