// @flow
import React from 'react'
import type { Match } from 'react-router-dom'

import Auth, { type User } from '../components/Auth'
import Resource from '../components/Resource'
import PostsPlaceholder from '../components/PostsPlaceholder'
import PostsList from '../components/PostsList'
import ErrorMessage from '../components/ErrorMessage'
import { getPosts } from '../../resources/posts'
import { allCategories } from '../../types/content'

const determineCategories = (user: ?User, match: Match) => {
  if (match.params.name) {
    const name = match.params.name

    // flow forces me to validate with an if statement so it
    // knows its an actual valid category name
    // @TODO: find a way around this
    if (
      name === 'politics' ||
      name === 'business' ||
      name === 'science' ||
      name === 'tech' ||
      name === 'sports'
    ) {
      return [name]
    } else {
      return []
    }
  } else if (user) {
    return user.interests.length ? user.interests : allCategories
  } else {
    return allCategories
  }
}

type Props = { match: Match }

const Main = ({ match }: Props) => (
  <Auth>
    {({ user }) => {
      const categories = determineCategories(user, match)

      return (
        <Resource get={getPosts}>
          {({ loading, error, data }) => {
            if (loading) return <PostsPlaceholder />

            if (error) return <ErrorMessage error={error} />

            if (data)
              return <PostsList posts={data.posts} categories={categories} />

            return null
          }}
        </Resource>
      )
    }}
  </Auth>
)

export default Main
