// @flow
import React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'

const Container = styled.article`
  max-width: 860px;
  margin: 100px auto 0;
  text-align: center;

  h1 {
    font-size: 32px;
  }

  p {
    font-size: 18px;
  }
`

const NotFound = () => (
  <Container>
    <Helmet>Page not found | Cheesecake</Helmet>

    <h1>There is nothing to be seen here!</h1>
    <p>
      We are sorry, there is no content matching this URL, try navigating again
      from the home page
    </p>
  </Container>
)

export default NotFound
