// @flow
import React from 'react'
import styled from 'styled-components'

type Props = { size: string, name: string, image: string }

const Container = styled.p`
  display: grid;
  grid-template-columns: ${({ size }) => size} 1fr;
  grid-gap: 10px;
  line-height: ${({ size }) => size};
  font-family: 'Open Sans', sans-serif;
  font-style: italic;

  span {
    display: block;
    width: ${({ size }) => size};
    height: ${({ size }) => size};
    overflow: hidden;
    border-radius: 50%;

    img {
      display: block;
      width: 100%;
      height: 100%;
    }
  }
`

const Author = ({ size, name, image }: Props) => (
  <Container size={size}>
    <span>
      <img src={image} alt={`${name}'s picture`} />
    </span>{' '}
    by {name}
  </Container>
)

export default Author
