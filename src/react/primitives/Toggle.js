// @flow
import React from 'react'
import styled from 'styled-components'

const Label = styled.label`
  color: ${({ color, checked }) => (checked ? '#fff' : color)};
  border: 1px solid ${({ color }) => color};
  background: ${({ checked, color }) => (checked ? color : '#fff')};
  display: inline-block;
  height: 28px;
  padding: 0 18px;
  border-radius: 14px;
  line-height: 28px;
  text-transform: uppercase;
  text-align: center;
  cursor: pointer;

  input {
    appearance: none;
    visibility: hidden;
    display: inline-block;
    width: 0px;
    height: 0px;
  }
`

type Props = {
  name: string,
  checked: boolean,
  onChange: (SyntheticInputEvent<HTMLInputElement>) => void,
  color: string,
}

const Toggle = ({ name, onChange, checked, color }: Props) => (
  <Label color={color} checked={checked}>
    <input
      name={name}
      id={name}
      type="checkbox"
      onChange={onChange}
      checked={checked}
    />
    {name}
  </Label>
)

export default Toggle
