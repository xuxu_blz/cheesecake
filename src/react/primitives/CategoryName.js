// @flow
import styled from 'styled-components'

import { categoryColors } from '../../types/content'

const CategoryName = styled.span.attrs({ children: props => props.category })`
  text-transform: uppercase;
  font-size: 10px;
  line-height: 24px;
  color: ${({ category }) => categoryColors[category]};
`

export default CategoryName
