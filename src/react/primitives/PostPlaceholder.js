// @flow
import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  filter: blur(3px);
  max-width: 860px;
  width: 100%;
  margin: 0 auto;
  padding-top: 6px;

  div {
    background: #ccc;
  }

  .placeholder__category {
    width: 36px;
    height: 10px;
    margin-bottom: 6px;
  }

  .placeholder__thumbnail {
    width: 100%;
    height: 0;
    padding-bottom: 63.888%;
    margin-bottom: 20px;
  }

  .placeholder__title {
    background: transparent;

    div {
      height: ${({ index }) => (index === 0 ? '26px' : '20px')};
      margin-bottom: 10px;
      width: 92%;

      &:last-child {
        width: 76%;
      }
    }
  }

  .placeholder__author {
    background: transparent;
    display: grid;
    grid-template-columns: ${({ index }) => (index === 0 ? '45px' : '32px')} 1fr;
    grid-gap: 10px;
    padding-top: 6px;
    margin-bottom: 20px;

    &:before,
    &:after {
      content: '';
      display: block;
      background: #ccc;
    }

    &:before {
      width: ${({ index }) => (index === 0 ? '45px' : '32px')};
      height: ${({ index }) => (index === 0 ? '45px' : '32px')};
      border-radius: 50%;
    }

    &:after {
      width: ${({ index }) => (index === 0 ? '30%' : '70%')};
      height: 18px;
      margin-top: ${({ index }) => (index === 0 ? '12px' : '6px')};
    }
  }

  .placeholder__text {
    background: transparent;

    div {
      height: 16px;
      margin-bottom: 6px;

      &:nth-child(1) {
        width: 80%;
      }

      &:nth-child(2) {
        width: 92%;
      }

      &:nth-child(3) {
        width: 73%;
      }

      &:nth-child(4) {
        width: 40%;
      }
    }
  }

  @media screen and (min-width: 480px) {
    grid-column: ${({ index }) => (index === 0 ? 'span 2' : 'span 1')};
  }

  @media screen and (min-width: 768px) {
    grid-column: ${({ index }) =>
      index === 0 ? 'span 6' : index < 3 ? 'span 3' : 'span 4'};
  }
`
type PlaceholderProps = { index: number }

const PostPlaceholder = ({ index }: PlaceholderProps) => (
  <Container index={index}>
    <div className="placeholder__category" />
    {index < 3 && <div className="placeholder__thumbnail" />}
    <div className="placeholder__title">
      <div />
      <div />
    </div>
    <div className="placeholder__author" />
    {index > 0 && (
      <div className="placeholder__text">
        <div />
        <div />
        <div />
        <div />
      </div>
    )}
  </Container>
)

export default PostPlaceholder
