// @flow
import styled from 'styled-components'

const Button = styled.button`
  display: block;
  transition: background 0.3s linear;
  background: ${({ theme }) => theme.primaryColor};
  width: 100%;
  max-width: 245px;
  height: 52px;
  font-size: 16px;
  margin: 0 auto 1em;
  text-transform: uppercase;
  color: #fff;
  border: 0;
  -moz-appearance: none;
  -webkit-appearance: none;
  cursor: pointer;

  &[disabled] {
    background: #999;
    cursor: default;
  }

  &[aria-busy='true'] {
    background: ${({ theme }) => theme.green};
  }
`

export default Button
