import React from 'react'

export const MockChild = () => <div className="mock-child" />

export class Call extends React.PureComponent {
  componentDidMount() {
    const { fn, withParams, immediately, after } = this.props

    if (immediately) {
      fn(withParams)
    } else if (after) {
      setTimeout(() => fn(withParams), after)
    }
  }

  render() {
    return null
  }
}
