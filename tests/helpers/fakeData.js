export const fakePosts = [
  {
    id: 'asdasdsadsadsa',
    title: 'Test post',
    excerpt: 'This is a test',
    thumbnail: 'http://google.com',
    author_name: 'Bruno Gama',
    author_image: 'http://google.com',
    category: 'sports',
  },
  {
    id: 'fdsjafkdçlsfds',
    title: 'Test post 2',
    excerpt: 'This is a test',
    thumbnail: 'http://google.com',
    author_name: 'Bruno Gama',
    author_image: 'http://google.com',
    category: 'tech',
  },
]

export const fakeFullPost = {
  id: '1mlHDOZoaz4vZgAuKfQ-wvTIeH6j7_2_r9A_0Sl6zFT0',
  title: 'Obama Offers Hopeful Vision While Noting Nation’s Fears',
  content:
    '<p>One day after Uber updated its API to add ‘content experiences’ for passengers, the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform up by releasing an SDK for developers and third-parties.</p><p>One day after Uber updated its API to add ‘content experiences’ for passengers, the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform up by releasing an SDK for developers and third-parties.</p>',
  thumbnail: 'https://i.imgur.com/X3f8Xw3.jpg',
  author_name: 'Bruno Gama',
  author_image:
    'https://en.gravatar.com/userimage/12881524/fd96754ef4205b829620aba5b056f520.jpeg',
  category: 'tech',
}
