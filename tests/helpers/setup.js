/* global Date: true global */
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import fetch from 'node-fetch'

// mock the date object
const RealDate = Date

Date = class extends RealDate {
  constructor() {
    super()
    return new RealDate('2018-06-25T12:00:00z')
  }
}

// mock the fetch API
global.fetch = fetch

// stub localstorage
export const localStorageStub = { getItem: () => null, setItem: () => {} }
global.localStorage = localStorageStub

configure({ adapter: new Adapter() })
