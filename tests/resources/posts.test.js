import nock from 'nock'

import { getPosts, getPost } from '../../src/resources/posts'

const base = 'https://sheets.googleapis.com'
const indexEndpoint = '/v4/spreadsheets/noID/values/A1:G31?key=noKey'
const singleEndpoint = '/v4/spreadsheets/asdasdsadsadsa/values/A1:G2?key=noKey'

const rawPostsResponse = {
  values: [
    [
      'id',
      'title',
      'excerpt',
      'thumbnail',
      'author_name',
      'author_image',
      'category',
    ],
    [
      'asdasdsadsadsa',
      'Test post',
      'This is a test',
      'http://google.com',
      'Bruno Gama',
      'http://google.com',
      'sports',
    ],
    [
      'fdsjafkdçlsfds',
      'Test post 2',
      'This is a test',
      'http://google.com',
      'Bruno Gama',
      'http://google.com',
      'tech',
    ],
  ],
}

const parsedPostsResponse = {
  posts: [
    {
      id: 'asdasdsadsadsa',
      title: 'Test post',
      excerpt: 'This is a test',
      thumbnail: 'http://google.com',
      author_name: 'Bruno Gama',
      author_image: 'http://google.com',
      category: 'sports',
    },
    {
      id: 'fdsjafkdçlsfds',
      title: 'Test post 2',
      excerpt: 'This is a test',
      thumbnail: 'http://google.com',
      author_name: 'Bruno Gama',
      author_image: 'http://google.com',
      category: 'tech',
    },
  ],
}

const rawPostResponse = {
  values: [
    [
      'id',
      'title',
      'content',
      'thumbnail',
      'author_name',
      'author_image',
      'category',
    ],
    [
      'asdasdsadsadsa',
      'Post 01',
      '<p>HTML content</p>',
      'https://google.com',
      'Bruno Gama',
      'https://google.com',
      'tech',
    ],
  ],
}

const parsedPostResponse = {
  posts: [
    {
      id: 'asdasdsadsadsa',
      title: 'Post 01',
      content: '<p>HTML content</p>',
      thumbnail: 'https://google.com',
      author_name: 'Bruno Gama',
      author_image: 'https://google.com',
      category: 'tech',
    },
  ],
}

describe('posts resource', () => {
  afterEach(() => nock.cleanAll())

  describe('getPosts()', () => {
    it('resolves with parsed response object', async () => {
      nock(base)
        .get(indexEndpoint)
        .reply(200, rawPostsResponse)

      const [_, response] = await getPosts()

      expect(response).toEqual(parsedPostsResponse)
    })
  })

  describe('getPost()', () => {
    it('resolves with parsed response object', async () => {
      nock(base)
        .get(singleEndpoint)
        .reply(200, rawPostResponse)

      const [_, response] = await getPost('asdasdsadsadsa')

      expect(response).toEqual(parsedPostResponse)
    })
  })
})
