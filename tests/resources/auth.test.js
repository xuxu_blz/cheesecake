import { logIn } from '../../src/resources/auth'

describe('auth resource', () => {
  it('resolves with user when data is valid', async () => {
    const [_, user] = await logIn({ username: 'nothing', password: '0000000' })

    expect(user).toEqual({
      id: 'a12nxcz38fad',
      name: 'Cheesecake',
      interests: [],
    })
  })

  it('resolves with error info when data is invalid', async () => {
    const [error] = await logIn({ username: 'something', password: '1111111' })

    expect(error.status).toEqual(401)
  })
})
