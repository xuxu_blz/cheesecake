import nock from 'nock'

import * as sheets from '../../src/resources/googleSheets'

const key = 'veryfaketoken'

const base = 'https://sheets.googleapis.com'
const endpoint = '/v4/spreadsheets/abc/values/A1:D21?key=veryfaketoken'

const fakeResponse = { values: [['id', 'name'], [1, 'a'], [2, 'b']] }
const parsedResponse = { posts: [{ id: 1, name: 'a' }, { id: 2, name: 'b' }] }

describe('getSheet', () => {
  afterEach(() => nock.cleanAll())

  it('sends request to correct URL with set params and token', async () => {
    const fn = jest.fn(() => {
      return fakeResponse
    })

    nock(base)
      .get(endpoint)
      .reply(200, fn)

    await sheets.fetchSheet({ id: 'abc', range: 'A1:D21', key })

    expect(fn).toBeCalledTimes(1)
  })

  it('resolves with parsed response object', async () => {
    nock(base)
      .get(endpoint)
      .reply(200, fakeResponse)

    const [_, response] = await sheets.fetchSheet({
      id: 'abc',
      range: 'A1:D21',
      key,
    })

    expect(response).toEqual(parsedResponse)
  })

  it('resolves with error object on failure', async () => {
    nock(base)
      .get(endpoint)
      .reply(500, fakeResponse)

    const [error] = await sheets.fetchSheet({
      id: 'abc',
      range: 'A1:D21',
      key,
    })

    expect(error.status).toEqual(500)
    expect(error.message).toBeTruthy()
  })
})
