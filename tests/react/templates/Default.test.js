import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import DefaultTemplate from '../../../src/react/templates/Default'

describe.only('Default template', () => {
  test('renders correctly', () => {
    const wrapper = shallow(
      <DefaultTemplate>
        <div>place children here</div>
      </DefaultTemplate>
    )

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
