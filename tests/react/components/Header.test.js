import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Header, { CategoryItem } from '../../../src/react/components/Header'

const categories = ['tech', 'politics']

describe.only('Header', () => {
  test('renders correctly', () => {
    const wrapper = shallow(
      <Header categories={categories} selectedCategory={null} />
    ).dive()

    expect(toJson(wrapper)).toMatchSnapshot()
  })

  test('highlights selected category', () => {
    const wrapper = shallow(
      <Header categories={categories} selectedCategory="politics" />
    ).dive()

    expect(
      wrapper
        .find(CategoryItem)
        .at(1)
        .prop('active')
    ).toBe(true)
  })
})
