import React from 'react'
import { render } from 'enzyme'

import Profile from '../../../src/react/components/Profile'

describe('Profile wrapper', () => {
  it('renders logged out layout', () => {
    expect(
      render(<Profile visible={true} user={null} logIn={() => {}} />)
    ).toMatchSnapshot()
  })

  it('renders logged in layout', () => {
    expect(
      render(
        <Profile
          visible={true}
          user={{ id: '123', name: 'Bruno Gama', interests: [] }}
          logOut={() => {}}
          updatePreferences={() => {}}
        />
      )
    ).toMatchSnapshot()
  })
})
