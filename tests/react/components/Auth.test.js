/* global global */
import React from 'react'
import { mount } from 'enzyme'
import { pipe, path, last, head } from 'ramda'
import waitForExpect from 'wait-for-expect'

import Auth, { AuthProvider } from '../../../src/react/components/Auth'
import { Call } from '../../helpers/components'
import { localStorageStub } from '../../helpers/setup'

const fakeUser = {
  id: '123123',
  name: 'Bruno Gama',
  interests: [],
}

const lastCall = pipe(
  path(['mock', 'calls']),
  last,
  head
)

describe('Auth', () => {
  afterEach(() => {
    global.localStorage = localStorageStub
  })

  it('passes data from provider to consumer', () => {
    const childFn = jest.fn(() => null)

    mount(
      <AuthProvider>
        <Auth>{childFn}</Auth>
      </AuthProvider>
    ).setState({ user: fakeUser })

    expect(lastCall(childFn).user).toEqual(fakeUser)
  })

  it('handles passed login function', () => {
    const wrapper = mount(
      <AuthProvider>
        <Auth>
          {({ logIn }) => <Call fn={logIn} withParams={fakeUser} immediately />}
        </Auth>
      </AuthProvider>
    )

    expect(wrapper.state('user')).toEqual(fakeUser)
  })

  it('saves data to localStorage on login', () => {
    const fn = jest.fn()
    global.localStorage.setItem = fn

    mount(
      <AuthProvider>
        <Auth>
          {({ logIn }) => <Call fn={logIn} withParams={fakeUser} immediately />}
        </Auth>
      </AuthProvider>
    )

    expect(fn).toBeCalledWith('_cheesecake_user', JSON.stringify(fakeUser))
  })

  it('handles passed logout function', async () => {
    const wrapper = mount(
      <AuthProvider>
        <Auth>{({ logOut }) => <Call fn={logOut} after={300} />}</Auth>
      </AuthProvider>
    ).setState({ user: fakeUser })

    await waitForExpect(() => {
      expect(wrapper.state('user')).toEqual(null)
    })
  })

  it('clears saved data on logout', async () => {
    const fn = jest.fn()
    global.localStorage.setItem = fn

    mount(
      <AuthProvider>
        <Auth>{({ logOut }) => <Call fn={logOut} after={300} />}</Auth>
      </AuthProvider>
    ).setState({ user: fakeUser })

    await waitForExpect(() => {
      expect(fn).toBeCalledWith('_cheesecake_user', JSON.stringify(null))
    })
  })

  it('loads saved data on mount', () => {
    const fn = jest.fn(name => {
      return (
        name === '_cheesecake_user' &&
        JSON.stringify({
          name: 'James Bond',
          id: '007',
          interests: [],
        })
      )
    })

    global.localStorage.getItem = fn

    const wrapper = mount(
      <AuthProvider>
        <Auth>{() => null}</Auth>
      </AuthProvider>
    )

    expect(wrapper.state('user')).toEqual({
      name: 'James Bond',
      id: '007',
      interests: [],
    })
  })

  it('handles passed updateInterests function', async () => {
    const wrapper = mount(
      <AuthProvider>
        <Auth>
          {({ updateInterests }) => (
            <Call
              fn={updateInterests}
              withParams={['politics', 'tech']}
              after={300}
            />
          )}
        </Auth>
      </AuthProvider>
    ).setState({ user: fakeUser })

    await waitForExpect(() => {
      expect(wrapper.state('user')).toEqual({
        ...fakeUser,
        interests: ['politics', 'tech'],
      })
    })
  })

  it('saves data to localStorage on updateInterests', async () => {
    const fn = jest.fn()
    global.localStorage.setItem = fn

    mount(
      <AuthProvider>
        <Auth>
          {({ updateInterests }) => (
            <Call
              fn={updateInterests}
              withParams={['politics', 'tech']}
              after={300}
            />
          )}
        </Auth>
      </AuthProvider>
    ).setState({ user: fakeUser })

    await waitForExpect(() => {
      expect(fn).toBeCalledWith(
        '_cheesecake_user',
        JSON.stringify({
          ...fakeUser,
          interests: ['politics', 'tech'],
        })
      )
    })
  })
})
