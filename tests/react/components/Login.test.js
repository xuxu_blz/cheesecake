import React from 'react'
import { render, mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'

import Login from '../../../src/react/components/Login'
import * as auth from '../../../src/resources/auth'

const logInFn = jest.fn(
  ({ username }) =>
    new Promise(resolve => {
      if (username === 'wrong_user') {
        resolve([{ status: 401, message: 'something wrong???' }, null])
      } else {
        resolve([null, { id: '123', name: 'Secret', interests: [] }])
      }
    })
)

auth.logIn = logInFn

describe('Login form', () => {
  it('renders correctly ', () => {
    expect(render(<Login onSuccess={() => {}} />)).toMatchSnapshot()
  })

  it('updates state with user input', () => {
    const wrapper = mount(<Login onSuccess={() => {}} />)

    wrapper.find('input[name="username"]').simulate('change', {
      target: { name: 'username', value: 'secret_user' },
    })

    wrapper.find('input[name="password"]').simulate('change', {
      target: { name: 'password', value: 'super_safe' },
    })

    expect(wrapper.state('username')).toEqual('secret_user')
    expect(wrapper.state('password')).toEqual('super_safe')
  })

  it('calls the login resource with current state on submit', () => {
    logInFn.mockClear()

    const wrapper = mount(<Login onSuccess={() => {}} />).setState({
      username: 'secret_user',
      password: 'super_safe',
    })

    wrapper.find('form').simulate('submit')

    expect(logInFn).toBeCalledWith({
      username: 'secret_user',
      password: 'super_safe',
    })
  })

  it('calls the passed function on success', async () => {
    logInFn.mockClear()
    const successFn = jest.fn()

    const wrapper = mount(<Login onSuccess={successFn} />).setState({
      username: 'secret_user',
      password: 'super_safe',
    })

    wrapper.find('form').simulate('submit')

    waitForExpect(() => {
      expect(successFn).toBeCalledWith({
        id: 'secret_user',
        name: 'super_safe',
        interests: [],
      })
    })
  })

  it('updates state on error', async () => {
    logInFn.mockClear()

    const wrapper = mount(<Login onSuccess={() => {}} />).setState({
      username: 'wrong_user',
      password: 'super_safe',
    })

    wrapper.find('form').simulate('submit')

    waitForExpect(() => {
      expect(wrapper.state('error').status).toEqual(401)
    })
  })

  it('calls passed function when close button is clicked', () => {
    const fn = jest.fn()

    mount(<Login onSuccess={() => {}} onClose={fn} />)
      .find('a')
      .simulate('click')

    expect(fn).toBeCalledTimes(1)
  })
})
