import React from 'react'
import { render, mount } from 'enzyme'

import Interests from '../../../src/react/components/Interests'

describe('Interests form', () => {
  it('renders correctly ', () => {
    expect(
      render(
        <Interests
          user={{ name: 'Bruno Gama', id: '123', interests: [] }}
          updateInterests={() => {}}
          logOut={() => {}}
        />
      )
    ).toMatchSnapshot()
  })

  it('updates state when interests checkboxes change', () => {
    const wrapper = mount(
      <Interests
        user={{ name: 'Bruno Gama', id: '123', interests: [] }}
        updateInterests={() => {}}
        logOut={() => {}}
      />
    )

    wrapper
      .find('input[type="checkbox"]')
      .first()
      .simulate('change', { target: { name: 'tech' } })
      .simulate('change', { target: { name: 'politics' } })
      .simulate('change', { target: { name: 'tech' } })

    expect(wrapper.state('interests')).toEqual(['politics'])
  })

  it('calls passed function when submitting', () => {
    const fn = jest.fn()

    const wrapper = mount(
      <Interests
        user={{ name: 'Bruno Gama', id: '123', interests: [] }}
        updateInterests={fn}
        logOut={() => {}}
        onClose={() => {}}
      />
    )

    wrapper.find('form').simulate('submit')

    expect(fn).toBeCalledTimes(1)
  })

  it('calls passed function when logout button is clicked', () => {
    const fn = jest.fn()

    const wrapper = mount(
      <Interests
        user={{ name: 'Bruno Gama', id: '123', interests: [] }}
        updateInterests={() => {}}
        logOut={fn}
      />
    )

    wrapper
      .find('a')
      .at(1)
      .simulate('click')

    expect(fn).toBeCalledTimes(1)
  })

  it('calls passed function when exit button is clicked', () => {
    const fn = jest.fn()

    const wrapper = mount(
      <Interests
        user={{ name: 'Bruno Gama', id: '123', interests: [] }}
        updateInterests={() => {}}
        logOut={() => {}}
        onClose={fn}
      />
    )

    wrapper
      .find('a')
      .first()
      .simulate('click')

    expect(fn).toBeCalledTimes(1)
  })
})
