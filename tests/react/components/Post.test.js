import React from 'react'
import { render } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'

import Post from '../../../src/react/components/Post'
import { fakePosts } from '../../helpers/fakeData'

describe('Post component', () => {
  it('renders first post layout ', () => {
    expect(
      render(
        <BrowserRouter>
          <Post post={fakePosts[0]} index={0} />
        </BrowserRouter>
      )
    ).toMatchSnapshot()
  })

  it('renders second and third post layout ', () => {
    expect(
      render(
        <BrowserRouter>
          <Post post={fakePosts[0]} index={1} />
        </BrowserRouter>
      )
    ).toMatchSnapshot()
  })

  it('renders fourth+ post layout', () => {
    expect(
      render(
        <BrowserRouter>
          <Post post={fakePosts[0]} index={3} />
        </BrowserRouter>
      )
    ).toMatchSnapshot()
  })
})
