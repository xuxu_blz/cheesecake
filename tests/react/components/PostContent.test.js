import React from 'react'
import { render } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'

import PostContent from '../../../src/react/components/PostContent'
import { fakeFullPost } from '../../helpers/fakeData'

describe('PostContent component', () => {
  it('renders correctly ', () => {
    expect(
      render(
        <BrowserRouter>
          <PostContent post={fakeFullPost} />
        </BrowserRouter>
      )
    ).toMatchSnapshot()
  })
})
