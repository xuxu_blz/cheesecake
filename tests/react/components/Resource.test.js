import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'

import Resource from '../../../src/react/components/Resource'

const render = Component => mount(Component)

const response = { success: true }
const error = { status: 500, message: 'Fake error' }

const successfulResource = () => Promise.resolve([null, response])

const errorResource = () => Promise.resolve([error, null])

const delayedResource = () =>
  new Promise(resolve => {
    setTimeout(() => resolve([null, response]), 300)
  })

describe('Resource component', function() {
  test('renders', function() {
    const wrapper = render(
      <Resource get={successfulResource}>{() => null}</Resource>
    )

    expect(wrapper).toBeTruthy
  })

  test('calls the get resource function with passed params', function() {
    const fn = jest.fn(successfulResource)
    const params = { fakeId: 'areyouacop' }

    render(
      <Resource get={fn} params={params}>
        {() => null}
      </Resource>
    )

    expect(fn).toBeCalledTimes(1)
  })

  test('calls the resource function again if the params prop changes', function() {
    const fn = jest.fn(successfulResource)
    const params = { fakeId: 'areyouacop' }

    const wrapper = render(
      <Resource get={fn} params={params}>
        {() => null}
      </Resource>
    )

    wrapper.setProps({ params: { fakeId: 'imtotally21' } })
    wrapper.setProps({ params: { fakeId: 'lookthatsmeinthepicture' } })
    wrapper.setProps({ params: { fakeId: 'whydontyoubelieveme' } })

    expect(fn).toBeCalledTimes(4)
  })

  test('does not call the resource function again if any other prop changes', function() {
    const fn = jest.fn(successfulResource)
    const params = { fakeId: 'areyouacop' }

    const wrapper = render(
      <Resource get={fn} params={params}>
        {() => null}
      </Resource>
    )

    wrapper.setProps({ otherProp: { test: 2 } })
    wrapper.setProps({ otherProp: { test: 3 } })
    wrapper.setProps({ otherProp: { test: 4 } })

    expect(fn).toBeCalledTimes(1)
  })

  test('passed data on request success', async function() {
    const childFn = jest.fn(() => null)

    render(<Resource get={successfulResource}>{childFn}</Resource>)

    await waitForExpect(() => {
      expect(childFn).toBeCalledWith({
        loading: false,
        error: null,
        data: response,
      })
    })
  })

  test('passes error on request error', async function() {
    const childFn = jest.fn(() => null)

    render(<Resource get={errorResource}>{childFn}</Resource>)

    await waitForExpect(() => {
      expect(childFn).toBeCalledWith({
        loading: false,
        error: error,
        data: null,
      })
    })
  })

  test('passes loading = true while promise is not resolved', async function() {
    const childFn = jest.fn(() => null)

    render(<Resource get={delayedResource}>{childFn}</Resource>)

    await waitForExpect(() => {
      expect(childFn).toBeCalledWith({
        loading: true,
        error: null,
        data: null,
      })
    })
  })
})
