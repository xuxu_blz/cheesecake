import React from 'react'
import { shallow, render } from 'enzyme'

import PostsList from '../../../src/react/components/PostsList'
import Post from '../../../src/react/components/Post'
import { fakePosts } from '../../helpers/fakeData'

describe('Posts list', () => {
  it('filters posts based on passed category list', () => {
    const wrapper = shallow(
      <PostsList posts={fakePosts} categories={['tech', 'politics']} />
    )

    expect(wrapper.find(Post).length).toBe(1)
  })

  it('renders informative text when there are no posts available', () => {
    expect(
      render(<PostsList posts={fakePosts} categories={[]} />)
    ).toMatchSnapshot()
  })
})
