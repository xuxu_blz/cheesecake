import React from 'react'
import { render } from 'enzyme'

import PostsPlaceholder from '../../../src/react/components/PostsPlaceholder'

describe('Posts placeholder component', () => {
  it('renders correct layout ', () => {
    expect(render(<PostsPlaceholder />)).toMatchSnapshot()
  })
})
