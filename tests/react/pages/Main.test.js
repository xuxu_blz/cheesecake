import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Main from '../../../src/react/pages/Main'

describe.only('Main page', () => {
  test('renders correctly', () => {
    const component = shallow(<Main />)

    expect(toJson(component)).toMatchSnapshot()
  })
})
