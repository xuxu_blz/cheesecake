import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Single from '../../../src/react/pages/Single'

describe.only('Single post', () => {
  test('renders correctly', () => {
    const component = shallow(<Single match={{ params: { id: 123 } }} />)

    expect(toJson(component)).toMatchSnapshot()
  })
})
