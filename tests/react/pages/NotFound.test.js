import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import NotFound from '../../../src/react/pages/NotFound'

describe.only('404 page', () => {
  test('renders correctly', () => {
    const component = shallow(<NotFound />)

    expect(toJson(component)).toMatchSnapshot()
  })
})
