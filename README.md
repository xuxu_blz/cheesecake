# Cheesecake Labs' frontend challenge - Bruno Gama's submission

## Project dependencies

This project should run fine in any operational system. The required software
is:

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)

## Running the project

It is required that you use a valid `.env` for the app to work correctly, the
required variables are listed in the `.env.sample` file and should be filled
with:

* `GOOGLE_API_KEY`: A valid Google API Key, with granted access to the Sheets API
  * you can find information on how to request your keys on the [Google API Console Help](https://support.google.com/googleapi/answer/6158862?hl=en)
  * the key provided with the env sample is valid (as of December 19, 2018)
* `GOOGLE_INDEX_SHEET_ID`: A google spreadsheet ID to be used as the API' index
  * The spreadsheet needs to follow the same format as the [example sheet](https://docs.google.com/spreadsheets/d/1eNmEYlZ0dT_5PhxFL15K7fkrp45xuo6OC0PNdLT2-zk/edit?usp=sharing)
* `VALID_USER`: A username to be used in the fake authentication request
* `VALID_PASS`: A password to be matched against in the fake authentication request

Install all dependencies with Yarn:

```bash
yarn install
```

### Development mode

Run:

```bash
yarn start
```

### Production mode

Run:

```bash
yarn serve
```

## Structure

The initial structure of this project was largely based on my starter project,
which is [available on my Github](https://github.com/bruno-gama/react-starter).

## Technology used

You can read a description of the technology used in this project and the reason
for each decision in the [technology.md](technology.md) file.
